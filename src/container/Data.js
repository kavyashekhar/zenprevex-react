import React, { Component } from 'react';
import axios from 'axios';

import Markets from '../components/Markets'

class Data extends Component {
    state = {
        
        data:[]
    }

    componentDidMount(){
            axios.get('http://localhost:8002/api/currency')
               .then(response =>{
                
                console.log(response)
                 this.setState({
                 
                  data:response.data.results
                 })
                 })
               .catch(error =>{
                console.log(error)
                
                  
               //this.setState({error:true});
               })
      }
    
       render(){

          
        return(
               
           <div> 
              
             <Markets data={this.state.data}  />
          </div>
                 
          )
       }
   }
   export default Data