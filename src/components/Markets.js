import React from 'react';


const markets= (props) => {

     
      
  

return(
   <div>
      <div >
      
      <table className="table table-striped">
      <tr>
      <h1>Markets</h1>
      </tr>
      <tr >
          <td><h4>currency</h4></td>
          <td><h4>change %24h</h4></td>
          <td><h4>market cap</h4></td>
          <td><h4>price USD</h4></td>
          <td><h4>supply</h4></td>
          <td><h4>volume</h4></td>
          
      </tr>
                       
            <tbody  >{props.data.map( (item, index,key) => {
                  

                     return (
                      
                      
                        <tr ><td>{item.currency_id}</td>
                            <td>{item.percent_change_24h}</td>
                            <td>{item.market_cap_usd}</td>
                            <td>{item.price_usd}</td>
                            <td>{item.available_supply}</td>
                            <td>{item.volume_usd_24h}</td>
                            
                        </tr>
                        
                      )
                   
                   })}</tbody>
             </table>
             </div>
      
        
	
	</div>)
}
export default markets;